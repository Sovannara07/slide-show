import 'package:flutter/material.dart';


class SliderModel{

  String imageAssetPath;
  String title;
  String desc;

  SliderModel({this.imageAssetPath,this.title,this.desc});

  void setImageAssetPath(String getImageAssetPath){
    imageAssetPath = getImageAssetPath;
  }

  void setTitle(String getTitle){
    title = getTitle;
  }

  void setDesc(String getDesc){
    desc = getDesc;
  }

  String getImageAssetPath(){
    return imageAssetPath;
  }

  String getTitle(){
    return title;
  }

  String getDesc(){
    return desc;
  }

}


List<SliderModel> getSlides(){

  List<SliderModel> slides = new List<SliderModel>();
  SliderModel sliderModel = new SliderModel();

  //1
  sliderModel.setDesc("An accommodating and versatile individual with the talent to develop inspiring hands-on lessons that will capture a child's imagination and breed success");
  sliderModel.setTitle("Teacher");
  sliderModel.setImageAssetPath("assets/teacher.jpg");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //2
  sliderModel.setDesc("The problem for students and teachers, however, is obvious: the background knowledge playing field is not equal");
  sliderModel.setTitle("Knowledge");
  sliderModel.setImageAssetPath("assets/knowledge.jpg");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  //3
  sliderModel.setDesc("In the course of professionalism and the increasing demand of competence orientation, the teaching and learning processes within the field of speech therapy stand at a profound stage of development");
  sliderModel.setTitle("Students");
  sliderModel.setImageAssetPath("assets/student.jpg");
  slides.add(sliderModel);

  sliderModel = new SliderModel();

  return slides;
}